# Do Your Part Bot

Made as a prank for fun. It spams the victim's Messenger (although it should work with any web chat) with "Subscribe to PewDiePie! https://www.youtube.com/watch?v=6Dh-RL__uN4" message.

## How does it work

You simply start the script, log into your Messenger account, select the victim and... Let the magic happen.

## System requirements

- Linux, macOS or Windows operating system
- [Chrome](https://www.google.com/chrome/)/[Chromium](https://www.chromium.org/getting-involved/download-chromium) browser
- [Python3](https://www.python.org/downloads/)
- [webbot](https://github.com/nateshmbhat/webbot)

## How do I run it

First of all you NEED to have [Python3](https://www.python.org/downloads/) and [webbot](https://github.com/nateshmbhat/webbot) installed. To install webbot, type in your terminal use:

```bash
pip3 install webbot
```

To start the tool, open it in your terminal, head to the directory in which you've unpacked the tool and type in:

```bash
python3 doyourpart.py
```

After that you just follow the instructions displayed in the terminal.

Remember that you need to have Chrome or Chromium browser installed to run that tool. It will not work with Firefox, Opera, Internet Explorer or Netscape Navigator!

## About

Made by [Konrad 'Athlon' Figura](https://twitter.com/_athl)