#!/usr/bin/python3

import sys

try:
    from webbot import Browser
except:
    print("webbot is missing! To install it, use 'pip3 install webbot' in your terminal.")
    sys.exit()


print("\nWelcome to Do Your Part Bot!")
web = Browser(showWindow=True)
web.go_to("https://www.messenger.com/")
print("Log into your Messenger account (or any other web chat) and select the target. To do that, just open the victim's chat.")
print("Remember that you can stop doing your part by pressing CTRL+C. This will stop the bot.")
input("Press ENTER to start doing your part!")
counter = 0

while True:
    web.type("Subscribe to PewDiePie! https://www.youtube.com/watch?v=6Dh-RL__uN4",)
    counter += 1
    print("You did your part " + str(counter) + " times.")
    web.press(web.Key.ENTER)
